﻿using Newtonsoft.Json;
using SourceS.Models.Helper;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace SourceS.Models
{
    [Table("dbo.Historicos")]
    [SoftDelete("IsDeleted")]
    public class Historico
    {
        [Key]
        public int historicoID { get; set; }
        [Required(ErrorMessage = ("Este campo es requerido"))]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy-MM-dd}")]
        [DataType(DataType.Date)]
        public DateTime fecha { get; set; }
        [Required(ErrorMessage = ("Este campo es requerido"))]
        [DataType(DataType.Time)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:HH:mm}")]
        public DateTime hora { get; set; }
        [Required(ErrorMessage = ("Este campo es requerido"))]
        [DataType(DataType.Currency)]
        [RegularExpression(@"^\d{2}\.\d{0,2}$", ErrorMessage = "Este campo es requerido con el siguiente formato 2 digitos y 2 decimales.")]
        public string precio { get; set; }
        public bool enviado { get; set; }
        public bool estatus { get; set; }
        public bool IsDeleted { get; set; }

        [ForeignKey("Dispositivo")]
        public int dispositivoID { get; set; }
        [JsonIgnore]
        public virtual Dispositivo Dispositivo { get; set; }
        [ForeignKey("Producto")]
        public int? ProductoId { get; set; }
        [JsonIgnore]
        public virtual Producto Producto { get; set; }
    }

}