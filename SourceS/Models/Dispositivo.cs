﻿using Newtonsoft.Json;
using SourceS.Models.CustomValidation;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace SourceS.Models
{    
    [Table("dbo.Dispositivos")]
    public class Dispositivo
    {
        [Key]
        [JsonProperty(Order = 1)]
        public int dispositivoID { get; set; }
        [Required(ErrorMessage =("Este campo es requerido"))]
        [JsonProperty(Order = 2)]
        [Display(Name =("Dirección IP"))]
        [IpAddress]
        public string ip { get; set; }
        [Required(ErrorMessage = ("Este campo es requerido"))]
        [JsonProperty(Order = 3)]
        [Display(Name = ("Nombre"))]
        [StringLength(25, MinimumLength = 3,ErrorMessage =("Este campo debe tener como minimo 3 caracteres y maximo 25"))]
        public string nombre { get; set; }
        [ForeignKey("Gasolinera")]
        [JsonProperty(Order = 4)]
        public int gasolineraID { get; set; }
        [JsonIgnore]
        public virtual Gasolinera Gasolinera { get; set; }
        [JsonProperty(Order = 5)]
        public virtual ICollection<Historico> Historicos { get; set; }

    }


}