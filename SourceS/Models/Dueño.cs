﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using SourceS.Models.Usuarios;

namespace SourceS.Models
{
    [Table("dbo.Dueños")]
    public class Dueño : Persona
    {

        public Dueño()
        {
            this.Gasolineras = new HashSet<Gasolinera>();
        }

        public bool Activado { get; set; }
        [ForeignKey("Administrador")]
        public string AdministradorId { get; set; }
        public Administrador Administrador { get; set; }
        public virtual ICollection<Gasolinera> Gasolineras { get; set; }
    }
}