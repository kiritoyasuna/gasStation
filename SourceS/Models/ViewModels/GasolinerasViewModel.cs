﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SourceS.Models.ViewModels
{
    public class GasolinerasViewModel
    {
        public Gasolinera Gasolinera { get; set; }
        public ICollection<Dispositivo> Dispositivos { get; set; }
        public int[] productosID { get; set; }
    }
}