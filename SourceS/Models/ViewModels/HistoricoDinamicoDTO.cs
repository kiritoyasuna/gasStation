﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SourceS.Models.ViewModels
{
    public class HistoricoDinamicoDTO
    {
        [Required(ErrorMessage = ("Este campo es requerido"))]
        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy-MM-dd}")]
        public DateTime Fecha { get; set; }
        [Required(ErrorMessage = ("Este campo es requerido"))]
        [DataType(DataType.Time)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:HH:mm}")]
        public DateTime Hora { get; set; }
        public int? gasolinerId { get; set; }
        public string identificador { get; set; }
        public List<Producto> Productos { get; set; }

    }
}