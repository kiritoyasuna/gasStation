﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SourceS.Models.ViewModels.UsuariosDTO
{
    public class RegistroDTO
    {
            [Required]
            [EmailAddress]
            [Display(Name = "Correo electrónico")]
            public string Email { get; set; }

            [Required]
            [StringLength(100, ErrorMessage = "El número de caracteres de {0} debe ser al menos {2}.", MinimumLength = 6)]
            [DataType(DataType.Password)]
            [Display(Name = "Contraseña")]
            public string Password { get; set; }

            [DataType(DataType.Password)]
            [Display(Name = "Confirmar contraseña")]
            [Compare("Password", ErrorMessage = "La contraseña y la contraseña de confirmación no coinciden.")]
            public string ConfirmPassword { get; set; }

            [Required(ErrorMessage = ("Este campo es requerido"))]
            [RegularExpression(@"^[a-zA-Z-Ññ\s]+$", ErrorMessage = "Solo se permiten letras  en estos campo")]
            [Display(Name = "Nombre")]
            [MaxLength(60)]
            public string Nombre { get; set; }

            [Required(ErrorMessage = ("Este campo es requerido"))]
            [RegularExpression(@"^[a-zA-Z-Ññ\s]+$", ErrorMessage = "Solo se permiten letras  en estos campo")]
            [Display(Name = "Apellido paterno")]
            [MaxLength(60)]
            public string ApellidoPaterno { get; set; }

            [Required(ErrorMessage = ("Este campo es requerido"))]
            [RegularExpression(@"^[a-zA-Z-Ññ\s]+$", ErrorMessage = "Solo se permiten letras  en estos campo")]
            [Display(Name = "Apellido materno")]
            [MaxLength(60)]
            public string ApellidoMaterno { get; set; }

            [DataType(DataType.PhoneNumber)]
            [RegularExpression(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$", ErrorMessage = "Número telefonico no válido")]
            [MaxLength(10)]
            [Display(Name = "Teléfono celular")]
            public string Celular { get; set; }

        }
}