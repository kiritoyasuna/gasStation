﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SourceS.Models.ViewModels
{
    public class DetallesDTO
    {
        public List<Gasolinera> Gasolineras { get; set; }
        public List<Producto> Productos { get; set;  }
    }
}