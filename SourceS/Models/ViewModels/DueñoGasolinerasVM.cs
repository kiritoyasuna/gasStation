﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SourceS.Models.ViewModels
{
    public class DueñoGasolinerasVM
    {

        public int gasolineraID { get; set; }
        public string identificador { get; set; }
        public string estado { get; set; }
        public string municipio { get; set; }
        public string calle { get; set; }
        public List<Dispositivo> Dispositivos { get; set; }
    }
}