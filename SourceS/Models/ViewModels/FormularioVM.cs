﻿using SourceS.Models.CustomValidation;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SourceS.Models.ViewModels
{
    public class FormularioVM
    {
        [IpAddress]
        [Required(ErrorMessage = ("Este campo es requerido"))]
        [Display(Name = ("Dirección IP"))]
        public string ip { get; set; }
        [Required(ErrorMessage = ("Este campo es requerido"))]
        [Display(Name = ("Nombre"))]
        [StringLength(25, MinimumLength = 3, ErrorMessage = ("Este campo debe tener como minimo 3 caracteres y maximo 25"))]
        public string nombre { get; set; }
        public List<ProductoDTO> Productos { get; set; }
        public int? gasolineraID {get;set;}
    }
}