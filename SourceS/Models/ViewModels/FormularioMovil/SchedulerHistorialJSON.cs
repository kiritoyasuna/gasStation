﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SourceS.Models.ViewModels.FormularioMovil
{
    public class SchedulerHistorialJSON
    {
        public List<HistorialesJSON> HistoricosToday { get; set; }
        public List<HistorialesJSON> Historicos { get; set; }
    }
}