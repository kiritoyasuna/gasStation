﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SourceS.Models;
using SourceS.Models.ViewModels;
using Newtonsoft.Json;
using System.Threading.Tasks;

namespace SourceS.Controllers
{
    public class DispositivosController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Dispositivos/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Dispositivo dispositivo = db.Dispositivos.Find(id);
            if (dispositivo == null)
            {
                return HttpNotFound();
            }
            return View(dispositivo);
        }


        // GET: Dispositivos/Edit/5
        [Authorize(Roles = ("Administrador"))]
        public ActionResult Edit(int? id)
        { 
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Dispositivo dispositivo = db.Dispositivos.Find(id);
            if (dispositivo == null)
            {
                return HttpNotFound();
            }
            return PartialView("_Edit",dispositivo);
        }

        // POST: Dispositivos/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = ("Administrador"))]
        public ActionResult Edit([Bind(Include = "dispositivoID,ip,nombre,gasolineraID")] Dispositivo dispositivo)
        {
            if (ModelState.IsValid)
            {
                dispositivo.Gasolinera = db.Gasolineras.Find(dispositivo.gasolineraID);
                db.Entry(dispositivo).State = EntityState.Modified;
                db.SaveChanges();
                return Json(new {success = true});
            }
            return PartialView("_Edit",dispositivo);
        }

        // GET: Dispositivos/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Dispositivo dispositivo = db.Dispositivos.Find(id);
            if (dispositivo == null)
            {
                return HttpNotFound();
            }
            return View(dispositivo);
        }

        // POST: Dispositivos/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id, int idGasolinera)
        {
            Dispositivo dispositivo = db.Dispositivos.Find(id);
            var gasolinera = db.Gasolineras.Find(idGasolinera);
            if (gasolinera.Dispositivos.Any())
            {
                if (gasolinera.Dispositivos.Contains(dispositivo))
                {
                    gasolinera.Dispositivos.Remove(dispositivo);
                }

            }
            db.Dispositivos.Remove(dispositivo);
            db.SaveChanges();
            return RedirectToAction("Delete", new { id = idGasolinera });
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
