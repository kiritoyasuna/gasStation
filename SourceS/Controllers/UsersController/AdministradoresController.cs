﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Security.Policy;
using System.Threading.Tasks;
using System.Web;
using System.Web.Management;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using SourceS.Models;
using SourceS.Models.Usuarios;
using SourceS.Models.ViewModels.UsuariosDTO;

namespace SourceS.Controllers.UsersController
{
    public class AdministradoresController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        private ApplicationUserManager _userManager;
        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        // GET: Administradores
        [Authorize(Roles = ("Admin"))]
        public ActionResult Index()
        {
            var administradores = db.Administradores.Include(a => a.ApplicationUser);
            return View(administradores.ToList());
        }

        // GET: Administradores/Details/5
        [Authorize(Roles = ("Admin,Administrador"))]
        public ActionResult Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Administrador administrador = db.Administradores.Find(id);
            if (administrador == null)
            {
                return HttpNotFound();
            }
            return PartialView("_Details", administrador);
        }

        // GET: Administradores/Create
        [Authorize(Roles = ("Admin"))]
        public ActionResult Create()
        {

            return PartialView("_Create");
        }

        // POST: Administradores/Create
        [Authorize(Roles = ("Admin"))]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(RegistroDTO registro)
        {
            if (ModelState.IsValid)
            {
                var user = new ApplicationUser { UserName = registro.Email, Email = registro.Email, EmailConfirmed = true, PhoneNumber = registro.Celular };
                var result = await UserManager.CreateAsync(user, registro.Password);

                //Create ApplicationUser
                if (result.Succeeded)
                {
                    await UserManager.AddToRoleAsync(user.Id, "Administrador");
                    var administrador = new Administrador();
                    administrador.personaID = user.Id;
                    administrador.ApplicationUser = db.Users.Find(user.Id);
                    administrador.Habilitado = false;
                    administrador.Creado = DateTime.Now.Date;
                    administrador.Nombre = registro.Nombre;
                    administrador.ApellidoPaterno = registro.ApellidoPaterno;
                    administrador.ApellidoMaterno = registro.ApellidoMaterno;
                    administrador.Celular = registro.Celular;

                    try
                    {
                        db.Administradores.Add(administrador);
                        db.SaveChanges();
                        return Json(new { success = true });
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e);
                        throw;
                    }
                }
            }

            return PartialView("_Create", registro);
        }

        // GET: Administradores/Edit/5
        [Authorize(Roles = ("Admin"))]
        public ActionResult Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Administrador administrador = db.Administradores.Find(id);
            if (administrador == null)
            {
                return HttpNotFound();
            }

            return PartialView("_Edit", administrador);
        }

        // POST: Administradores/Edit/5
        [Authorize(Roles = ("Admin"))]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Administrador administrador)
        {
            if (ModelState.IsValid)
            {
                var user =  UserManager.FindById(administrador.personaID);

                user.Email = administrador.ApplicationUser.Email;
                user.UserName = administrador.ApplicationUser.Email;
                UserManager.Update(user);

                administrador.ApplicationUser.Id = user.Id;
                administrador.ApplicationUser = db.Users.Find(user.Id);
                administrador.Creado = DateTime.Now.Date;


                db.Entry(administrador).State = EntityState.Modified;
                db.SaveChanges();
                return Json(new {success = true});
            }
            return PartialView("_Edit", administrador);
        }

        // GET: Administradores/Delete/5
        [Authorize(Roles = ("Admin"))]
        public ActionResult Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Administrador administrador = db.Administradores.Find(id);
            if (administrador == null)
            {
                return HttpNotFound();
            }
            return PartialView("_Delete", administrador);
        }

        // POST: Administradores/Delete/5
        [Authorize(Roles = ("Admin"))]
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
            try
            {

                Administrador administrador = db.Administradores.FirstOrDefault(x => x.personaID == id);
                db.Administradores.Attach(administrador);
                db.Administradores.Remove(administrador);

                var user = db.Users.AsNoTracking().FirstOrDefault(X => X.Id == id);
                db.Users.Attach(user);
                db.Users.Remove(user);
                db.SaveChanges();
                return Json(new { success = true });
            }
            catch (Exception e)
            {
                throw;
            }

        }

        #region ON/OFF
        public JsonResult _ControlMaestro(string administradorId)
        {
            //Deshabilitar al Administrador -> Apagar Todos lo Usuarios del Admin
            var administrador = db.Administradores.Find(administradorId);
            if (administrador.Habilitado == false)
            {
                administrador.Habilitado = true;
            }

            if (administrador.Habilitado == true)
            {
                administrador.Habilitado = false;
            };

            try
            {
                db.Administradores.Attach(administrador);
                db.Entry(administrador).State = EntityState.Modified;
                foreach (var usuarios in administrador.Dueños)
                {
                    if (administrador.Habilitado == false)
                    {
                        usuarios.Activado = true;
                    }

                    if (administrador.Habilitado == true)
                    {
                        usuarios.Activado = false;
                    }
                   
                }

                db.SaveChanges();
                return Json(new { success = true },JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                throw;
            }
        }
        

        #endregion

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
