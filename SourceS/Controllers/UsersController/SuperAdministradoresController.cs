﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SourceS.Models;
using SourceS.Models.Usuarios;

namespace SourceS.Controllers.UsersController
{
    public class SuperAdministradoresController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: SuperAdministradores
        public ActionResult Index()
        {
            var superAdministradores = db.SuperAdministradores.Include(s => s.ApplicationUser);
            return View(superAdministradores.ToList());
        }

        // GET: SuperAdministradores/Details/5
        public ActionResult Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SuperAdministrador superAdministrador = db.SuperAdministradores.Find(id);
            if (superAdministrador == null)
            {
                return HttpNotFound();
            }
            return View(superAdministrador);
        }

        // GET: SuperAdministradores/Create
        public ActionResult Create()
        {
            
            return View();
        }

        // POST: SuperAdministradores/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "personaID,Creado,Nombre,ApellidoPaterno,ApellidoMaterno,Celular")] SuperAdministrador superAdministrador)
        {
            if (ModelState.IsValid)
            {
                db.SuperAdministradores.Add(superAdministrador);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

         
            return View(superAdministrador);
        }

        // GET: SuperAdministradores/Edit/5
        public ActionResult Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SuperAdministrador superAdministrador = db.SuperAdministradores.Find(id);
            if (superAdministrador == null)
            {
                return HttpNotFound();
            }
           
            return View(superAdministrador);
        }

        // POST: SuperAdministradores/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "personaID,Creado,Nombre,ApellidoPaterno,ApellidoMaterno,Celular")] SuperAdministrador superAdministrador)
        {
            if (ModelState.IsValid)
            {
                db.Entry(superAdministrador).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
           
            return View(superAdministrador);
        }

        // GET: SuperAdministradores/Delete/5
        public ActionResult Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SuperAdministrador superAdministrador = db.SuperAdministradores.Find(id);
            if (superAdministrador == null)
            {
                return HttpNotFound();
            }
            return View(superAdministrador);
        }

        // POST: SuperAdministradores/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
            SuperAdministrador superAdministrador = db.SuperAdministradores.Find(id);
            db.SuperAdministradores.Remove(superAdministrador);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
