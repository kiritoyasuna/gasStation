﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using SourceS.Models;
using SourceS.Models.ViewModels;

namespace SourceS.Controllers.WebServices
{
    [RoutePrefix("api/ProductosJS")]
    public class ProductosJSController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();


        // GET: api/Productos
      

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ProductoExists(int id)
        {
            return db.Productos.Count(e => e.ProductoId == id) > 0;
        }
    }
}