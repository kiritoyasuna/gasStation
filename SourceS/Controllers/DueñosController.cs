﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SourceS.Models;
using Microsoft.AspNet.Identity.Owin;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using SourceS.Models.Usuarios;
using SourceS.Models.ViewModels;
using SourceS.Models.ViewModels.ApagadoDevice;

namespace SourceS.Controllers
{
    public class DueñosController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        private ApplicationUserManager _userManager;
        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        #region Administrador
        // GET: Dueños
        [Authorize(Roles = ("Administrador,Admin"))]
        public async Task<ActionResult> Index()
        {
            var userId = User.Identity.GetUserId();
            if (userId == null)
            {
                return HttpNotFound();
            }
            var usuarios =  await db.Administradores.AsNoTracking().Where(a => a.personaID == userId).SelectMany(d => d.Dueños)
                .OrderBy(n => n.Nombre).ToListAsync();

            return View(usuarios);
        }
        // GET: Dueños/Create

        [Authorize(Roles = ("Administrador"))]
        public ActionResult Create()
        {
            return PartialView("_Create");
        }

        // POST: Dueños/Create
        [HttpPost]
        [Authorize(Roles = ("Administrador"))]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(RegisterViewModel registro)
        {
            if (ModelState.IsValid)
            {
                var user = new ApplicationUser { UserName = registro.Email, Email = registro.Email, EmailConfirmed = true, PhoneNumber = registro.celular };
                var result = await UserManager.CreateAsync(user, registro.Password);
                var userId = User.Identity.GetUserId();


                //Create ApplicationUser
                if (result.Succeeded)
                {
                    await UserManager.AddToRoleAsync(user.Id, "Usuario");
                    var dueño = new Dueño();
                    dueño.personaID = user.Id;
                    dueño.ApplicationUser = db.Users.Find(user.Id);
                    dueño.Nombre = registro.nombre;
                    dueño.ApellidoPaterno = registro.apellidoPaterno;
                    dueño.ApellidoMaterno = registro.apellidoMaterno;
                    dueño.Celular = registro.celular;
                    dueño.Gasolineras = new List<Gasolinera>();


                    // TODO: Add insert logic here
                    db.Dueños.Add(dueño);
                    //Add to Admin Role
                    var administrador = db.Administradores.Find(userId);
                    administrador.Dueños.Add(dueño);

                    db.SaveChanges();
                    return Json(new {success = true});
                }

                AddErrors(result);
            }

            return PartialView("_Create",registro);

        }

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }

        // GET: Dueños/Details/5
        public ActionResult Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Dueño dueño = db.Dueños.Find(); ;
            if (dueño == null)
            {
                return HttpNotFound();
            }
            return View(dueño);
        }


        // GET: Dueños/Edit/5
        [Authorize(Roles = ("Administrador"))]
        public ActionResult Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Dueño dueño = db.Dueños.Find(id);
            if (dueño == null)
            {
                return HttpNotFound();
            }
            return PartialView("_Edit",dueño);
        }

        // POST: Dueños/Edit/5
        [Authorize(Roles = ("Administrador"))]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(Dueño dueño)
        {
            var administrador = db.Administradores.AsNoTracking().Include(d => d.Dueños).FirstOrDefault(a => a.personaID == dueño.AdministradorId);        
            //One to Many
            var user = await UserManager.FindByIdAsync(dueño.personaID);
         
            user.Email = dueño.ApplicationUser.Email;
            user.UserName = dueño.ApplicationUser.Email;
            await UserManager.UpdateAsync(user);
            dueño.ApplicationUser.Id = user.Id;
            dueño.ApplicationUser = db.Users.Find(user.Id);
            dueño.Administrador = administrador;

            if (ModelState.IsValid)
            {
                db.Entry(dueño).State = EntityState.Modified;
                AddedUsuario(dueño.personaID,administrador);
                db.SaveChanges();
                return Json(new { success = true });
            }
            return PartialView("_Edit", dueño);
        }

        public void AddedUsuario(string usuarioId, Administrador administrador)
        {
            var usuarios = new HashSet<string>(administrador.Dueños.Select(d => d.personaID));

            foreach (var usuario in db.Dueños.AsNoTracking())
            {
                if (usuario.personaID == usuarioId)
                {

                    if (usuarios.Contains(usuario.personaID))
                    {
                        administrador.Dueños.Remove(usuario);
                    }

                    if (usuarios.Contains(usuario.personaID))
                    {
                        administrador.Dueños.Add(usuario);
                    }
                }
            }

        }


        // GET: Dueños/Delete/5
        [Authorize(Roles = ("Administrador"))]
        public ActionResult Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Dueño dueño = db.Dueños.Find(id);
            if (dueño == null)
            {
                return HttpNotFound();
            }
            return PartialView("_Delete",dueño);
        }

        // POST: Dueños/Delete/5
        [Authorize(Roles = ("Administrador"))]
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(string id)
        {      
            var dueño = await  db.Dueños.FirstOrDefaultAsync(d => d.personaID == id);
            db.Dueños.Attach(dueño);
            db.Dueños.Remove(dueño);

            var user = db.Users.AsNoTracking().FirstOrDefault(X => X.Id == id);
            db.Users.Attach(user);
            db.Users.Remove(user);
            db.SaveChanges();

            /* if (dueño.Gasolineras != null && dueño.Gasolineras.Any())
             {
                 DeleteGasolineras(dueño.Gasolineras.ToList(), dueño);
             }
             db.Dueños.Remove(dueño);*/
            try
            {
                db.SaveChanges();
                return Json(new {success = true});

            } catch(Exception E)
            {
                Console.WriteLine(E);
            }
            return PartialView("_Delete", dueño);
        }

        public void DeleteGasolineras(List<Gasolinera> gasolineras,Dueño dueño)
        {
            foreach (var gasolinera in gasolineras)
            {
                if (dueño.Gasolineras.Contains(gasolinera))
                {
                    dueño.Gasolineras.Remove(gasolinera);
                }
            }
        }


        public JsonResult _ControlOFF(string usuarioId)
        {
            //Deshabilitar al Administrador -> Apagar Todos lo Usuarios del Admin
            var usuario = db.Dueños.Find(usuarioId);
            usuario.Activado = true;
            db.Dueños.Attach(usuario);
            db.Entry(usuario).State = EntityState.Modified;
            //OFF Gasolineras
            db.SaveChanges();
            return Json(new { success = true }, JsonRequestBehavior.AllowGet);
        }


        public JsonResult _ControlON(string usuarioId)
        {
            //Deshabilitar al Administrador -> Apagar Todos lo Usuarios del Admin
            var usuario = db.Dueños.Find(usuarioId);
            usuario.Activado = false;
            db.Dueños.Attach(usuario);
            db.Entry(usuario).State = EntityState.Modified;
            //OFF Gasolineras
            db.SaveChanges();
            return Json(new { success = true }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Apagado(string id)
        {
            var Dueño = db.Dueños.Find(id);
            if (Dueño.Activado == true)
            {
                //*Logica para desactivar apagado
                //Modified Dueño
                Dueño.Activado = false;
                //Dispositivos del Cliente
                if (ModelState.IsValid)
                {
                    var conexion = new ClientSocket();
                    var devices = Dueño.Gasolineras.SelectMany(d => d.Dispositivos);
                    foreach (var dispositivo in devices)
                    {
                        var historico = new ProgramaDto();
                        {
                            historico.ip = dispositivo.ip;
                            historico.identificador = 0;
                            historico.fecha = DateTime.Now.ToString("MM/dd/yyyy");
                            historico.hora = DateTime.Now.ToString("HH:mm");
                            historico.precio = "0";
                            historico.estatus = "False";
                            historico.comando = "";
                            historico.baudio = 9600;
                            historico.apagado = "Reactivado";
                        }
                        conexion.ClientThreadStart(historico);
                    }
                    db.Entry(Dueño).State = EntityState.Modified;
                    db.SaveChanges();
                }
                return RedirectToAction("Index");
            }
            //Modified Dueño
            Dueño.Activado = true;
            //Dispositivos del Cliente
            if (ModelState.IsValid)
            {
                var conexion = new ClientSocket();
                var devices = Dueño.Gasolineras.SelectMany(d => d.Dispositivos);
                foreach (var dispositivo in devices)
                {
                    var historico = new ProgramaDto();
                    {
                        historico.ip = dispositivo.ip;
                        historico.identificador = 0;
                        historico.fecha = DateTime.Now.ToString("MM/dd/yyyy");
                        historico.hora = DateTime.Now.ToString("HH:mm");
                        historico.precio = "0";
                        historico.estatus = "False";
                        historico.comando = "";
                        historico.apagado = "True";
                    }
                    conexion.ClientThreadStart(historico);
                }
                db.Entry(Dueño).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.valor = false;

            return View();
        }

        #endregion

        #region SuperAdministrador
        [Authorize(Roles = ("Admin"))]
        public ActionResult Usuarios(string administradorId)
        {
            var dueños = db.Administradores.AsNoTracking().Where(x => x.personaID == administradorId).SelectMany(d => d.Dueños).OrderBy(n => n.Nombre);

            return View(dueños);
        }


        #endregion

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
