﻿using Microsoft.AspNet.Identity;
using SourceS.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System.Globalization;

namespace SourceS.Controllers
{
    public class HomeController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

   
        //Get/Historicos
        [HttpGet]
        public ActionResult Index()
        {

            if (User.IsInRole("Admin"))
            {
                return RedirectToAction("Admin", "Home");
            }

            if (User.IsInRole("Administrador"))
            {
                return RedirectToAction("Administrador", "Home");
            }

            if (User.IsInRole("Usuario"))
            {
                return RedirectToAction("Usuario", "Home");
            }

            return RedirectToAction("Login","Account");
        }

        [Authorize(Roles = ("Admin"))]
        public ActionResult Admin()
        {
            var userId = User.Identity.GetUserId();

            if (userId == null)
            {
                return HttpNotFound();
            }

            var historicos = db.Historicos.AsNoTracking();

            var dispositivos = db.Dispositivos.AsQueryable().Count();
            var dueños = db.Dueños.AsQueryable().Count();
            var ejecutados = historicos.Count(x => x.estatus == true);
            var noejecutados = historicos.Count(x => x.estatus == false);


            ViewBag.Cuentas = getUsuarios(null);
            ViewBag.Administradores = getAdministradores();
            ViewBag.Dispositivos = getDispositivos();
            ViewBag.Usuarios = getUsuariosEstaciones(null);
            ViewBag.execute = getProgramasEjecutados();
            ViewBag.notexecute = getProgramasEnEspera();


            return View(historicos);
        }


        [HttpPost]
        [ActionName("Admin")]
        public ActionResult ConfirmAdmin(string intervalDi,string intervalDf,string Cuentas)
        {
            ViewBag.Cuentas = getUsuarios(Cuentas);

            var historicos = new List<Historico>();
            if (String.IsNullOrEmpty(intervalDi) && String.IsNullOrEmpty(intervalDf)) { TempData["Message"] = "Los campos de fechas son requeridos"; return View(historicos); };


            var fechaInicio = DateTime.Parse(intervalDi).Date;
            var fechaFinal = DateTime.ParseExact(intervalDf,"dd/mm/yyyy", CultureInfo.CreateSpecificCulture("es-ES")).Date;

            historicos = db.Historicos.Where(h => h.fecha >= fechaInicio && h.fecha <= fechaFinal).ToList();

            ViewBag.Administradores = getAdministradores();
            ViewBag.Dispositivos = getDispositivos();
            ViewBag.Usuarios = getUsuariosEstaciones(null);
            ViewBag.execute = getProgramasEjecutados();
            ViewBag.notexecute = getProgramasEnEspera();


            return View(historicos);
        }


        #region Methods
        public SelectList getUsuarios(string parametro)
        {
            var usuarios = new SelectList(db.Administradores.AsNoTracking(), "nombre", "personaId", parametro);

            return usuarios;
        }

        public int getUsuariosEstaciones(string administradorId)
        {
            var usuarios = new int();
            if (administradorId == null) { usuarios = db.Dueños.AsNoTracking().Count(); return usuarios; }

            usuarios = db.Dueños.Where(admin => admin.AdministradorId == administradorId).AsQueryable().Count();

            return usuarios;
        }

        public int getAdministradores()
        {
            var administradores = db.Administradores.AsNoTracking().Count();

            return administradores;
        }


        public int getDispositivos()
        {
            var dispositivos = db.Dispositivos.AsNoTracking().Count();
            return dispositivos;
        }

        public IList<Historico> getHistoriales()
        {
            var historiales = db.Historicos.AsNoTracking().ToList();
            return historiales;
        }

        public int getProgramasEjecutados()
        {
            var programas = getHistoriales();
            return programas.Count(p => p.estatus == true);
        }

        public int getProgramasEnEspera()
        {
            var programas = getHistoriales();
            return programas.Count(p => p.estatus == false);
        }


        public JsonResult GetCuenta(string query)
        {
            var administradores = db.Administradores.Where(a => a.Nombre.Contains(query)).Select(a => new
            {
                id = a.personaID,
                text = a.Nombre
            });


            return Json(administradores, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Users
        [Authorize(Roles = ("Usuario"))]
        public ActionResult Usuario()
        {
            var userID = User.Identity.GetUserId();
            if (userID == null)
            {
                return HttpNotFound();
            }
            //Historial Dueño
            var Historicos = db.Dueños.AsNoTracking().Where(x => x.personaID == userID).SelectMany(g => g.Gasolineras).SelectMany(d => d.Dispositivos).SelectMany(h => h.Historicos).OrderBy(h => h.fecha);

            return View(Historicos.ToList());
        }


        [HttpPost]
        [Authorize(Roles = ("Usuario"))]
        public ActionResult Usuario(string intervalDI, string intervalDF)
        {
            var historicos = new List<Historico>();

            if (String.IsNullOrEmpty(intervalDI) && String.IsNullOrEmpty(intervalDF))
            {
                TempData["Message"] = "Los campos de fechas son requeridos";

            }
            else if (!String.IsNullOrEmpty(intervalDI) && String.IsNullOrEmpty(intervalDF))
            {

                TempData["Message"] = "El campo fecha final es requerido";

            }
            else if (String.IsNullOrEmpty(intervalDI) && !String.IsNullOrEmpty(intervalDF))
            {
                TempData["Message"] = "El campo fecha Inicial es requerido";
            }
            else if (!String.IsNullOrEmpty(intervalDI) && !String.IsNullOrEmpty(intervalDF))
            {
                var fecha = DateTime.Parse(intervalDI);
                var fechaF = DateTime.Parse(intervalDF);

                if (DateTime.Compare(fecha, fechaF) >= 0)
                {
                    TempData["Message"] = "La Fecha inicial no puede ser mayor que la fecha final";
                }
                else
                {
                    var userId = User.Identity.GetUserId();
                    if (userId == null) return HttpNotFound();

                    var historia = db.Dueños.Where(x => x.personaID == userId).SelectMany(f => f.Gasolineras)
                        .SelectMany(d => d.Dispositivos).SelectMany(h => h.Historicos.AsQueryable());
                    var historial = historia.Where(h => h.fecha >= fecha && h.fecha <= fechaF);
                    historicos.AddRange(historial);
                }

            }

            return View(historicos.ToList());
        }
        #endregion

        #region Administradores
        [HttpGet]
        [Authorize(Roles = ("Administrador"))]
        public ActionResult Administrador()
        {
            return View();
        }

        [Authorize(Roles = ("Administrador"))]
        [HttpPost]
        public ActionResult Administrador(string intervalID, string intervalED)
        {
            return View();
        }
        #endregion

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

    }

}