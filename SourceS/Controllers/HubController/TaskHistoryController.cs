﻿using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using SourceS.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web;

namespace SourceS.Controllers.HubController
{
    [HubName("taskHub")]
    public class TaskHistoryController: Hub
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        private readonly TimeSpan _updateInterval = TimeSpan.FromMilliseconds(1000);
        private static int currentId = 0;
        private Timer _timer;


        public List<Historico> GetHistoricos()
        {
            var historicos = db.Historicos.Where(x => x.estatus == false).ToList(); 

            return historicos;
        }
       
        public void UpdateTask()
        {
            _timer = new Timer(UpdateTaskService, null,1000,1000);
        }

        private void UpdateTaskService(Object state)
        {

            Clients.All.updateTaskService(GetHistoricos());
        }
    }
}