﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using SourceS.Models;
using SourceS.Models.ViewModels;
using System.Data.Entity.Infrastructure;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using SourceS.Helpers;
using SourceS.Models.FormularioAdministrador;

namespace SourceS.Controllers
{
    public class GasolinerasController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        #region Admin
        // GET: Gasolineras
        [Authorize(Roles=("Usuario"))]
        public ActionResult Index()
        {

            var usuarioId = User.Identity.GetUserId();
            if (usuarioId == null)
            {
                return HttpNotFound();
            }
            var dueño = db.Dueños.Find(usuarioId);
            if (dueño == null)
            {
                return HttpNotFound();
            }

            var gasolineras =dueño.Gasolineras.ToList();
            ViewBag.userName = dueño.Nombre;

            return View(gasolineras);
        }

        [Authorize(Roles = ("Administrador"))]
        public async Task<ActionResult> Lista(string id)
        {

            if (String.IsNullOrEmpty(id)) { return new HttpStatusCodeResult(404); }

            var viewModel = new List<DueñoGasolinerasVM>();
            var persona = await db.Dueños.Where(x => x.personaID == id).Include(t => t.Gasolineras).FirstOrDefaultAsync();

            ViewBag.personaID = persona.personaID;
            ViewBag.Nombre = persona.Nombre;

            return View(viewModel);
        }

        public JsonResult getData(string id)
        {
            var viewModels = new List<DueñoGasolinerasVM>();

            var gasolineraViewModel = db.Dueños.AsQueryable().Where(x => x.personaID == id).Include(t => t.Gasolineras)
                .SelectMany(g => g.Gasolineras);

            foreach (var model in gasolineraViewModel)
            {
                var dispositivos = model.Dispositivos.Select(d => new Dispositivo { dispositivoID = d.dispositivoID, ip = d.ip, nombre = d.nombre, gasolineraID = d.gasolineraID });
                viewModels.Add(new DueñoGasolinerasVM
                {
                    gasolineraID = model.gasolineraID,
                    identificador = model.identificador,
                    estado = model.estado,
                    municipio = model.municipio,
                    calle = model.calle,
                    Dispositivos = dispositivos.ToList()

                });

            }

            if (gasolineraViewModel != null)
            {

                return Json(new { data = viewModels }, JsonRequestBehavior.AllowGet);

            }
            return Json(new { data = "" }, JsonRequestBehavior.AllowGet);

        }

        #region Crear Gasolinera
        public ActionResult Formulario(string usuarioId)
        {
            var opt = Option(1);
            ViewBag.Opt = opt;
            ViewBag.usuarioIDE = usuarioId;
            return View();
        }

        public MultipleStepProgressTabOption Option(int currentIndex)
        {
            var opt = new MultipleStepProgressTabOption()

            {

                Steps = new List<string>()

                {

                    "Crear Gasolinera",

                    "Crear Dispositivo",

                    "Crear Producto"

                },

                CurrentStepIndex = currentIndex

            };

            return opt;
        }

        [HttpPost]
        [Authorize(Roles = ("Administrador"))]
        public ActionResult Step1(string usuarioId, Gasolinera gasolinera)
        {
            gasolinera.identificador = GenerateRandomNo(gasolinera.estado, gasolinera.municipio, gasolinera.colonia, gasolinera.calle);
            var dueño = db.Dueños.Find(usuarioId);

            if (ModelState.IsValid)
            {
                var opt = Option(2);
                ViewBag.Opt = opt;
                ViewBag.gasolineraID = new SelectList(db.Gasolineras.Where(x => x.gasolineraID == gasolinera.gasolineraID), "gasolineraID", "identificador");
                db.Gasolineras.Add(gasolinera);
                dueño.Gasolineras.Add(gasolinera);

                db.SaveChanges();
                return PartialView("_Step2");
            }

            var opt1 = Option(1);
            ViewBag.Opt = opt1;

            return PartialView("Formulario", gasolinera);
        }

        [HttpPost]
        [Authorize(Roles = ("Administrador"))]
        public ActionResult Step2(DispositivoDTO dispositivo,int gasolineraID,CantidadProductos CantidadProductos)
        {
            var gasolinera = db.Gasolineras.Find(gasolineraID);
            if (gasolinera != null)
            {
                if (ModelState.IsValid)
                {

                    var device = new Dispositivo()
                    {
                        ip = dispositivo.ip,
                        nombre = dispositivo.nombre,
                        Gasolinera = gasolinera,
                        gasolineraID = gasolineraID,
                    };

                    var opt = Option(3);
                    ViewBag.Opt = opt;
                    db.Dispositivos.Add(device);
                    gasolinera.Dispositivos.Add(device);
                    db.SaveChanges();
                    //Enum Cantidad Productos
                    var lista = GetProductos(CantidadProductos);
                    ViewBag.idGasolinera = gasolineraID;
                    //Model Lista productos
                    return PartialView("_Step3",lista);
                }
            }

            //Ocurs Error
            var opt1 = Option(2);
            ViewBag.Opt = opt1;
            ViewBag.gasolineraID = new SelectList(db.Gasolineras.Where(x => x.gasolineraID == gasolinera.gasolineraID), "gasolineraID", "identificador");
            return PartialView("_Step2", dispositivo);
        }

        public IList<Producto> GetProductos(CantidadProductos enumerable)
        {
            var producto = new List<Producto>();
            var productos = new List<Producto>();
            if (enumerable == CantidadProductos.Uno)
            {
                producto = new List<Producto>()
            {
                new Producto() {
                Nombre = "Producto",
                Precio = "10.00",
                Letra = "A",
                IsDeleted  = false,
                RowVersion = DateTime.Now.Date}
            };
                productos.AddRange(producto);
            }
            if (enumerable == CantidadProductos.Dos)
            {
                producto = new List<Producto>()
            {
                new Producto() {
                Nombre = "Producto",
                Precio = "10.00",
                Letra = "A",
                IsDeleted  = false,
                RowVersion = DateTime.Now.Date},
                 new Producto() {
                Nombre = "Producto",
                Precio = "10.00",
                Letra = "B",
                IsDeleted  = false,
                RowVersion = DateTime.Now.Date}
            };
                productos.AddRange(producto);
            }
            if (enumerable == CantidadProductos.Tres)
            {
                producto = new List<Producto>()
            {
                new Producto() {
                Nombre = "Producto",
                Precio = "10.00",
                Letra = "A",
                IsDeleted  = false,
                RowVersion = DateTime.Now.Date},
                 new Producto() {
                Nombre = "Producto",
                Precio = "10.00",
                Letra = "B",
                IsDeleted  = false,
                RowVersion = DateTime.Now.Date},
                new Producto() {
                Nombre = "Producto",
                Precio = "10.00",
                Letra = "C",
                IsDeleted  = false,
                RowVersion = DateTime.Now.Date}
            };
                productos.AddRange(producto);
            }
            if (enumerable == CantidadProductos.Cuatro)
            {
                producto = new List<Producto>()
            {
                new Producto() {
                Nombre = "Producto",
                Precio = "10.00",
                Letra = "A",
                IsDeleted  = false,
                RowVersion = DateTime.Now.Date},
                 new Producto() {
                Nombre = "Producto",
                Precio = "10.00",
                Letra = "B",
                IsDeleted  = false,
                RowVersion = DateTime.Now.Date},
                new Producto() {
                Nombre = "Producto",
                Precio = "10.00",
                Letra = "C",
                IsDeleted  = false,
                RowVersion = DateTime.Now.Date},
                 new Producto() {
                Nombre = "Producto",
                Precio = "10.00",
                Letra = "D",
                IsDeleted  = false,
                RowVersion = DateTime.Now.Date}
            };
                productos.AddRange(producto);
            }
            if (enumerable == CantidadProductos.Cinco)
            {
                producto = new List<Producto>()
            {
                new Producto() {
                Nombre = "Producto",
                Precio = "10.00",
                Letra = "A",
                IsDeleted  = false,
                RowVersion = DateTime.Now.Date},
                 new Producto() {
                Nombre = "Producto",
                Precio = "10.00",
                Letra = "B",
                IsDeleted  = false,
                RowVersion = DateTime.Now.Date},
                new Producto() {
                Nombre = "Producto",
                Precio = "10.00",
                Letra = "C",
                IsDeleted  = false,
                RowVersion = DateTime.Now.Date},
                 new Producto() {
                Nombre = "Producto",
                Precio = "10.00",
                Letra = "D",
                IsDeleted  = false,
                RowVersion = DateTime.Now.Date},
                new Producto() {
                Nombre = "Producto",
                Precio = "10.00",
                Letra = "E",
                IsDeleted  = false,
                RowVersion = DateTime.Now.Date}
            };
                productos.AddRange(producto);
            }


            return productos;
        }



        [HttpPost]
        [Authorize(Roles = ("Administrador"))]
        public ActionResult Step3(int? gasolineraId, List<Producto> productos)
        {
            //Obligatorio Relacion Producto -Gasolinera

            string[] Letra = new string[] { "A","B","C","D","E"};

            if (ModelState.IsValid)
            {
                var gasolinera = db.Gasolineras.Find(gasolineraId);

                for(var I = 0; I < productos.Count(); I ++)
                {
                    productos[I].RowVersion = DateTime.Now.Date;
                    productos[I].GasolineraId = gasolinera.gasolineraID;
                    productos[I].Gasolinera = gasolinera;
                    productos[I].Letra = Letra[I];
                }

                db.Productos.AddRange(productos);
                db.SaveChanges();
                return RedirectToAction("Index","Dueños");
            }

            var opt = Option(3);
            ViewBag.Opt = opt;
            return PartialView("_Step3",productos);
        }

        #endregion


        // GET: Gasolineras/Details/5
        [Authorize(Roles = ("Administrador"))]
        public ActionResult Details(int? id,string Andromeda)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Gasolinera gasolinera = db.Gasolineras.Find(id);
            ViewBag.Androide = Andromeda;
            if (gasolinera == null)
            {
                return HttpNotFound();
            }
            return View(gasolinera);
        }


        public string GenerateRandomNo(string Estado, string Municipio, string Colonia, string Calle)
        {

            string id = "";
            //input[0].ToString();
            string estado = Estado[0].ToString().ToUpper(); ;
            string municipio = Municipio[0].ToString().ToUpper(); ;
            string colonia = Colonia[0].ToString().ToUpper(); ;
            string calle = Calle[0].ToString().ToUpper(); ;


            int _min = 1000;
            int _max = 9999;
            Random _rdm = new Random();
            var random = _rdm.Next(_min, _max).ToString(); ;

            id = random + "-" + estado + municipio + colonia + calle;

            return id;
        }


        // GET: Gasolineras/Edit/5
        [HttpGet]
        [Authorize(Roles =("Administrador"))]
        public ActionResult Edit(int? id, string Andromeda)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Gasolinera gasolinera = db.Gasolineras.Find(id);
            if (gasolinera == null)
            {
                return HttpNotFound();
            }

            ViewBag.ID = Andromeda;

            return PartialView("_Edit",gasolinera);
        }

        // POST: Gasolineras/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles =("Administrador"))]
        public ActionResult Edit([Bind(Include = "gasolineraID,identificador,ciudad,municipio,estado,colonia,calle,idDueño")] Gasolinera gasolinera, string Andromeda)
        {

            if (ModelState.IsValid)
            {
                gasolinera.identificador = GenerateRandomNo(gasolinera.estado, gasolinera.municipio, gasolinera.colonia, gasolinera.calle);
                gasolinera.Dueño = db.Dueños.Find(Andromeda);

                db.Entry(gasolinera).State = EntityState.Modified;
                #region Logica Usuario           
                var dueño = db.Dueños.FirstOrDefault(d => d.personaID == Andromeda);
                if (dueño != null)
                {
                    UpdateEstacionesEdit(gasolinera.gasolineraID, dueño);
                }
                #endregion

                db.SaveChanges();
                return Json(new { success = true, message = gasolinera.identificador + ": se actualizo su información" }, JsonRequestBehavior.AllowGet );
            }
            return PartialView("_Edit",gasolinera);
        }

        public void UpdateEstacionesEdit(int gasolineraID, Dueño personaUpdate)
        {
            var personaEstaciones = new HashSet<int>(personaUpdate.Gasolineras.Select(c => c.gasolineraID));

            foreach (var gasolinera in db.Gasolineras)
            {
                if (gasolineraID == gasolinera.gasolineraID)
                {
                    if (personaEstaciones.Contains(gasolinera.gasolineraID))
                    {
                        personaUpdate.Gasolineras.Remove(gasolinera);
              
                    }

                    if (personaEstaciones.Contains(gasolinera.gasolineraID))
                    {
                        personaUpdate.Gasolineras.Add(gasolinera);
                    }
                }
            }

        }



        // GET: Gasolineras/Delete/5
        [Authorize(Roles = ("Administrador"))]
        public PartialViewResult Delete(int? id, string Andromeda)
        {
            Gasolinera gasolinera = db.Gasolineras.Find(id);
            ViewBag.idPersona = Andromeda;
            return PartialView("_Delete",gasolinera);
        }

        // POST: Gasolineras/Delete/5
        [Authorize(Roles =("Administrador"))]
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id, string Andromeda)
        {
            var persona = db.Dueños.Find(Andromeda);
            Gasolinera gasolinera = db.Gasolineras.Find(id);
            if (persona.Gasolineras.Contains(gasolinera))
            {
                persona.Gasolineras.Remove(gasolinera);
            }

            try
            {
                db.Gasolineras.Remove(gasolinera);
                db.SaveChanges();
                return Json(new { success = true, message = gasolinera.identificador + ": eliminado del sistema" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new { success = false, message = "Ha ocurrido un error en el sistema" + e.ToString() }, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion


        //Detalles de Gasolinera y Productos
        [Authorize(Roles = ("Admin"))]
        public ActionResult Detalles(string usuarioId)
        {
            if (usuarioId == null) { return HttpNotFound(); }



            /* var detalles = new DetallesDTO()
             {

             };

             //gasolineras y Producto*/

            return View();
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
