﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(SourceS.Startup))]
namespace SourceS
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            app.MapSignalR();
            ConfigureAuth(app);
        }
    }
}
