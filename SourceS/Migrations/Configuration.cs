namespace SourceS.Migrations
{
    using Models;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
            ContextKey = "SourceS.Models.ApplicationDbContext";
        }

        protected override void Seed(ApplicationDbContext context)
        {
            bool success = false;
            var idManager = new ApplicationDbContext.IdentityManager();
            success = idManager.CreateRole("Admin");
            success = idManager.CreateRole("Administrador");
            success = idManager.CreateRole("Usuario");
            ApplicationUser Administrador = new ApplicationUser()
            {
                UserName = "admin@admin.com",
                Email = "admin@admin.com",
                EmailConfirmed = true
            };

            success = idManager.CreateUser(Administrador, "Password1.");
            success = idManager.AddUserToRole(Administrador.Id, "Admin");
        }
    }
}
