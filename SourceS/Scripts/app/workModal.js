﻿$(function () {
    $("a[data-modal=parts]").on("click", function () {
        $("#partsModalContent").load(this.href, function () {
            $("#partsModal").modal({ keyboard: true }, "show");

            $("#partchoice").submit(function () {
                if ($("#partchoice").valid()) {
                    $.ajax({
                        url: this.action,
                        type: this.method,
                        data: $(this).serialize(),
                        success: function (result) {
                            if (result.success) {
                                $("#partsModal").modal("hide");
                                toastr.success(result.message);
                                location.reload();
                            } else {
                                $("#MessageToClient").text("La informacion no fue actualizada.");
                            }
                        },
                        error: function () {
                            $("#MessageToClient").text("El sistema tuvo un error, consulte con su administrador.");
                        }
                    });
                    return false;
                }
            });
        });
        return false;
    });

    $("a[data-modal=details]").on("click", function () {
        $("#detailsModalContent").load(this.href, function () {
            $("#detailsModal").modal({ keyboard: true }, "show");

            $("#detailschoice").submit(function () {
                if ($("#detailschoice").valid()) {
                    $.ajax({
                        url: this.action,
                        type: this.method,
                        data: $(this).serialize(),
                        success: function (result) {
                            if (result.success) {
                                $("#detailsModal").modal("hide");
                                toastr.success(result.message);
                                location.reload();
                            } else {
                                $("#MessageToClient").text("La informacion no fue actualizada.");
                            }
                        },
                        error: function () {
                            $("#MessageToClient").text("El sistema tuvo un error, consulte con su administrador.");
                        }
                    });
                    return false;
                }
            });
        });
        return false;
    });

    $("a[data-modal=programs]").on("click", function () {
        $("#programsModalContent").load(this.href, function () {
            $("#programsModal").modal({ keyboard: true }, "show");

            $("#programschoice").submit(function () {
                if ($("#programschoice").valid()) {
                    $.ajax({
                        url: this.action,
                        type: this.method,
                        data: $(this).serialize(),
                        success: function (result) {
                            if (result.success) {
                                $("#programsModal").modal("hide");
                                setTimeout(function() {
                                    swal("Exito!", "Ha registrado un programa!", "success");
                                    location.reload();
                                }, 3000);
                            } else {
                                $("#MessageToClient").text("La informacion no fue actualizada.");
                            }
                        },
                        error: function () {
                            $("#MessageToClient").text("El sistema tuvo un error, consulte con su administrador.");
                        }
                    });
                    return false;
                }
            });
        });
        return false;
    });
});