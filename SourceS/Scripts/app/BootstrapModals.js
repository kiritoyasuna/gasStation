﻿
$(function () {
    $.ajaxSetup({ cache: false });
    $("a[data-modal]").on("click", function (e) {
        $('#myModalContent').load(this.href, function () {
            $('#myModal').modal({
                /*backdrop: 'static',*/
                keyboard: true
            }, 'show');

            bindForm(this);
        });

        return false;
    });

});

function bindForm(dialog) {
    console.log("Que traes"+ dialog)
    $('form', dialog).submit(function () {
        $.ajax({
            url: this.action,
            type: this.method,
            data: $(this).serialize(),
            success: function (result) {
                if (result.success) {
                    $('#myModal').modal('hide');
                    toastr.success(result.message);
                    //Refresh Table
                    $("#example").DataTable().ajax.reload();
                } else {
                    //$('#myModal').html(result);
                    $("#MyModal").modal('show');
                    toastr.error(result.ErrorMessage );
                   // bindForm(dialog);
                }
            },
            error: function (xml, message, text) {
                toastr.error("Msg: " + message + ", Text: " + text);
            }
        });
        return false;
    });
}